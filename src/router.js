import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)
export function createRouter() {
  return new Router({
    mode: 'history',
    routes: [
      {
        path: '/',
        name: 'home',
        component: () => import('./views/Home.vue')
      },
			{
        path: '/buy',
        name: 'buy',
        component: () => import('./views/Buy.vue')
      }
    ],
	  scrollBehavior() {
	      return {x: 0, y: 0}
	  }
  })
}
