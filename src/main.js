import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import moment from 'moment'
import { createRouter } from './router'
import { createStore } from './store'
import { sync } from  'vuex-router-sync'
import './register-sw'

export function createApp() {
	const router = createRouter()
	const store = createStore()
	sync(store, router)
	const app = new Vue({
		router,
		store,
		render: h => h(App),
	})
	return { app, router, store }
}
