<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
header("Access-Control-Allow-Origin: *");

session_start();

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

$test = true;
$entityId = "8acda4c973fb2a40017410324d6d36f2"; $bearerToken = "OGE4Mzk0YzQ1YzM0OTYzNTAxNWMzYWJlODQ5NzM0Njh8Uk5lZzZXV21Rbg==";
if ($test == true) { $entityId = "8ac7a4c973fb7c850173fd45c8f60a6b"; $bearerToken = "OGFjN2E0Yzk3M2ZiN2M4NTAxNzNmZDFmZmViODA5ZDd8U2dROURHcWZhNA=="; }
$errors = 0;
$date = date("yyyymmddhhss");
if (isset($_POST['total'])) {
	$request = initial_request($_POST['total'], '0736036007');
	$response = json_decode($request, true);
	$checkoutId = $response['id'];
}
if (isset($_POST['cart'])) {
	$fp = fopen('orders/order.csv', 'w');
	$arr = json_decode($_POST['cart'], true);
	foreach ($arr as $line) {
		fputcsv($fp, $line);
	}
	fclose($fp);
}

$message = '';

if (isset($_GET['resourcePath'])) {
	$message = '';
	$payment_status = check_status ($_GET['resourcePath']);
	$payment_array = json_decode ($payment_status, true);

	if (substr($payment_array['result']['code'], 0, 2) == "000") {
		if (substr($payment_array['result']['code'], 4, 6) != "400" or substr($payment_array['result']['code'], 4, 6) != "100") {
			$message .= "<p>Thank you! Your order has been processed. We will confirm your order via email.</p>";
			// send mail and process CSV
			$body = json_encode($payment_array);
			$mail = new PHPMailer(true);
			try {
				$mail->CharSet = 'UTF-8';
				$mail->Host       = "https://smtp.satctracker.co.za";
				$mail->SMTPDebug  = 0;
				$mail->SMTPAuth   = true;
				$mail->Port       = 587;
				$mail->Username   = "do-not-reply";
				$mail->Password   = "S@T123tracker@";
				$mail->SMTPSecure = 'ssl';
		    $mail->setFrom('do-not-reply@yellow-card.co.za', 'Mailer');
		    $mail->addAddress('neil.bromehead@gmail.com');
		    $mail->addAttachment('orders/order.csv');
		    $mail->isHTML(true);
		    $mail->Subject = 'Yellow Card - Order';
		    $mail->Body = $body;
		    $mail->send();
			} catch (Exception $e) {
				$success = 0;
			}
		}
	} else {
		$message .= "<p>Unfortunately, we were unable to process your payment.</p>";
	}
}
?>

<!doctype html>
<html lang="en">
  <head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  </head>
	<body class="bg-light">
		<header>
		  <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
		    <div class="container">
					<img src="/img/comesa-logo.png" style="width: 150px;">
		      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
		        <span class="navbar-toggler-icon"></span>
		      </button>
		      <div class="collapse navbar-collapse " id="navbarNavDropdown">
		        <ul class="navbar-nav ms-auto">
							<li class="nav-item">
		            <a class="nav-link" href="#">Need help?</a>
		          </li>
							<li class="nav-item">
		            <a class="nav-link" href="#">Contact us</a>
		          </li>
		        </ul>
		      </div>
		    </div>
		  </nav>
		</header>
		<div class="container-fluid bg-dark text-light mb-5">
			<div class="row">
				<div class="col-12">
					<div class="container">
						<div class="row pt-5">
							<div class="col-12 col-md-12 text-center">
								<br/><h3 class="pt-5 pb-4"><strong>Complete your payment</strong></h3>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid mt-5 bg-light text-center">
			<div class="row">
				<?php print $message; ?>
				<div class="col-md-6 p-5">
					<h3 class="pb-3"><strong>Order</strong> details</h3>
					<?php
						$display = json_decode($_POST['cart'], true);
						print "<p>" . "<strong>" . $display[0]['company'] . "</strong> "  . $display[0]['email'] . " " . $display[0]['mobile'] .  "</p>";
					?>
					<table width="100%">
						<?php
						foreach ($display as $line) {
						?>
						<tr style="border: 1px solid #ccc; margin: 10px;">
							<td align="left"><strong>Destination</strong></td>
							<td align="left"><strong>Vehicle Type</strong></td>
							<td align="left"><strong>Reg Number</strong></td>
							<td align="left"><strong>Chassis Number</strong></td>
						</tr>
						<tr>
							<td align="left">
								<?php print $line['country']; ?>
							</td>
							<td align="left">
								<?php print $line['vehicle']; ?>
							</td>
							<td align="left">
								<?php print $line['reg']; ?>
							</td>
							<td align="left">
								<?php print $line['chassis']; ?>
							</td>
						</tr>
						<?php
						}
						?>
					</table>
					<h4 class="mt-3">Total: $<?php print $_POST['total']; ?></h4>
				</div>
				<div class="col-md-6 p-5">
					<h3 class="pb-3"><strong>Payment</strong> details</h3>
					<script src="https://test.oppwa.com/v1/paymentWidgets.js?checkoutId=<?php print $checkoutId; ?>"></script>
					<form action="http://comesa.stratitude-develop.co.za/payments/payment-engine.php" class="paymentWidgets" data-brands="VISA MASTER AMEX"></form>
				</div>
			</div>
		</div>
		<div class="subfooter bg-black py-3 text-light text-muted text-center">
			<div class="container">
				<span><small>Indwe COMESA Yellow Card | All rights reserved © 2022 | Edit Cookie Preferences | <a href="privacy-policy">Privacy Policy</a></small></span>
			</div>
		</div>
	</body>
</html>

<?php

function initial_request($amount, $phone) {

	global $entityId, $bearerToken, $test;

	if ($test == 1) {
		$url = "https://test.oppwa.com/v1/checkouts";
  } else {
  	$url = "https://oppwa.com/v1/checkouts";
  }

	$data = "entityId=" . $entityId .
				"&amount=" . $amount .
				"&currency=ZAR" .
				"&paymentType=DB" .
				"&merchantTransactionId=" . $phone;

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                   'Authorization:Bearer ' . $bearerToken));
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$responseData = curl_exec($ch);
	if(curl_errno($ch)) {
		return curl_error($ch);
	}
	curl_close($ch);
	return $responseData;
}

function check_status($path) {

	global $entityId, $bearerToken, $test;

	if ($test == 1) {
	$url = "https://test.oppwa.com" . $path;
	} else {
	$url = "https://oppwa.com" . $path;
	}
	$url .= "?entityId=" . $entityId;

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                   'Authorization:Bearer ' . $bearerToken));
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$responseData = curl_exec($ch);
	if(curl_errno($ch)) {
		return curl_error($ch);
	}
	curl_close($ch);
	return $responseData;
}
?>
